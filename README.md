#SETUP

    1.  Install Node if you already haven't


    2.  Download D3

        You can download the zip file or directly link the latest verion of the library in your code.       
        https://d3js.org/
        

    3.  Install D3 nodules: 
        npm i -SE d3-scale
        npm i -SE d3-svg-legend
        npm i -SE d3-geo
        npm i -SE d3-array
        npm i -SE d3-selection
        npm i -SE d3-brush
        npm i -SE d3-transition
        npm i -SE d3-axis
        npm i -SE d3-interpolate


    4.  Clone this repository   
        git clone https://bitbucket.org/knara/data-visualization


    6.  Navigate to the project file and run to test
        cd <FILE NAME>
        npm start


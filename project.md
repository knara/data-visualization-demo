#EDITING GRAPHICS

        1.  Change direction of gradient or fully change fill of bars.
    
    
        2.  Change amount of bars in the graph and corresponding labels.
    
    
        3.  Create a separate data document (data.js, data.csv, etc) and have the bar graph data linked through that.
    
   
#ETC

        1.  Create your own data visualization app from scratch.
            npm install -g create-react-app
            create-react-app my-app
            cd my-app
            npm start
      

        2.  Add necessary "chart.js" file 
            This can technically be any kind of chart // bar graphs are definitely simpler 
        
        
        3.  Edit App.js file & make sure that the graphic is referenced in said file
        
        
        4.  Add to App.css file if necessary 

import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import BarChart from './RadarChart'



class App extends Component {
  render() {
  return (

     <div className='App'>
        <div className='App-header'>
            <font size="7">A D3 DEMONSTRATION</font>
            <p>HOW MUCH DO I WANT THIS SEMESTER TO END?</p>
        </div>

        <div>
        <BarChart data={[5,0,6,0,9,0,9.9,0,10,0,3,0,5.5,0,10,0,10,0,12]} size={[500,500]} />
        </div>
</div>
  )
  }
}

export default App;


import React, { Component } from "react"
import './App.css'
import { select } from "d3-selection"

import { scaleLinear } from "d3-scale"
import { max } from "d3-array"



class BarChart extends Component {
   constructor(props){
      super(props)
      this.createBarChart = this.createBarChart.bind(this)
   }

   componentDidMount() {
      this.createBarChart()
   }

   componentDidUpdate() {
      this.createBarChart()
   }


   createBarChart() {
      const node = this.node
      const dataMax = max(this.props.data)
      const yScale = scaleLinear()
         .domain([0, dataMax])
         .range([0, this.props.size[1]])

   select(node)
      .selectAll('rect')
      .data(this.props.data)
      .enter()
      .append('rect')

   select(node)
      .selectAll('rect')
      .data(this.props.data)
      .exit()
      .remove()

   select(node)
      .selectAll('rect')
      .data(this.props.data)
      .style('fill', 'url(#MyGradient)')
      .attr('x', (d,i) => i * 25)
      .attr('y', d => this.props.size[1] - yScale(d))
      .attr('height', d => yScale(d))
      .attr('width', 35)

      .on("mouseover", function(d) {
        select(this).style("fill", 'url(#GradientTwo)');
      })

      .on("mouseout", function(d) {
      select(this).style("fill", 'url(#MyGradient)');
      })



   }



render() {


      return <svg ref={node => this.node = node}
      width={800} height={700}>
      <defs>
              <linearGradient id="MyGradient" x1="0" x2="0" y1="0" y2="1">
                  <stop offset="0%"  stop-color="#f69084"/>
                  <stop offset="100%" stop-color="#fff1ad"/>
              </linearGradient>


              <linearGradient id="GradientTwo" x1="0" x2="0" y1="0" y2="1">
                  <stop offset="0%"  stop-color="#D7EF93"/>
                  <stop offset="100%" stop-color="#93D9EF"/>
              </linearGradient>

          </defs>
          <text x="0" y="550" fill="gray" font-size="9px">AUG</text>
          <text x="50" y="550" fill="gray" font-size="9px">SEP</text>
          <text x="100" y="550" fill="gray" font-size="9px">OCT</text>
          <text x="150" y="550" fill="gray" font-size="9px">NOV</text>
          <text x="200" y="550" fill="gray" font-size="9px">DEC</text>
          <text x="250" y="550" fill="gray" font-size="9px">JAN</text>
          <text x="300" y="550" fill="gray" font-size="9px">FEB</text>
          <text x="350" y="550" fill="gray" font-size="9px">MAR</text>
          <text x="400" y="550" fill="gray" font-size="9px">APR</text>
          <text x="450" y="550" fill="gray" font-size="9px">MAY</text>
          <text x="210" y="650" fill="gray" font-size="20px">MONTH</text>
          <text x="600" y="275" fill="gray" font-size="20px">PERCENTAGE</text>
      </svg>
   }
}

export default BarChart
